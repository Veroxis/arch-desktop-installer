mod packages;
mod services;
mod scripts;

use packages::*;
use services::*;
use scripts::*;

use std::io;
use std::io::{Write};

#[derive(Debug)]
enum Desktops {
    NONE,
    DWM,
    GNOME,
    XFCE,
    KDE,
    CINNAMON,
}

#[derive(Debug)]
enum Packages {
    NONE,
    MINIMAL,
    DEFAULT,
}

fn main() {
    print_logo();
    let (chosen_desktop, chosen_packages) = query_user_selections();
    install_yay_package_manager();
    install_arch_base_system();
    let mut package_list: PackageList = vec![];
    let mut service_list: ServiceList = vec![];
    populate_package_list(&chosen_desktop, &chosen_packages, &mut package_list);
    populate_service_list(&chosen_desktop, &chosen_packages, &mut service_list);
    println!("-- Installing Desktop --");
    install_package_list(&package_list);
    handle_service_list(&service_list);
    execute_post_install_hooks(&chosen_desktop, &chosen_packages);
}

fn print_logo() {
    println!("    _             _       ____            _    _              ");
    println!("   / \\   _ __ ___| |__   |  _ \\  ___  ___| | _| |_ ___  _ __  ");
    println!("  / _ \\ | '__/ __| '_ \\  | | | |/ _ \\/ __| |/ / __/ _ \\| '_ \\ ");
    println!(" / ___ \\| | | (__| | | | | |_| |  __/\\__ \\   <| || (_) | |_) |");
    println!("/_/   \\_\\_|  \\___|_| |_| |____/ \\___||___/_|\\_\\\\__\\___/| .__/ ");
    println!("                                                       |_|    ");
    println!(" ___           _        _ _           ");
    println!("|_ _|_ __  ___| |_ __ _| | | ___ _ __ ");
    println!(" | || '_ \\/ __| __/ _` | | |/ _ \\ '__|");
    println!(" | || | | \\__ \\ || (_| | | |  __/ |   ");
    println!("|___|_| |_|___/\\__\\__,_|_|_|\\___|_|   ");
    println!("                                      ");
}

fn query_user_selections() -> (Desktops, Packages) {
    let mut chosen_desktop = Desktops::NONE;
    let mut chosen_packages = Packages::NONE;
    let mut valid_selection: bool = false;
    let mut user_input: String;

    // Packages
    println!("--");
    println!("Package [NONE]:    Contains zero packages. Used to skip package installation.");
    println!("Package [MINIMAL]: Contains basic services like fonts, filesystem drivers etc.");
    println!("Package [DEFAULT]: Contains standard desktop applications like an WebBrowser,");
    println!("                   an pdf viewer or an office suite.");
    println!();
    while !valid_selection {
        user_input = String::new();
        println!("Which package do you want to install?");
        print!("# ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        std::io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to read line");
        user_input = user_input.to_uppercase();
        match user_input.trim() {
            "NONE" => {
                println!();
                println!("You chose the package [none]");
                chosen_packages = Packages::NONE;
                valid_selection = true;
            }
            "MINIMAL" => {
                println!();
                println!("You chose the package [minimal]");
                chosen_packages = Packages::MINIMAL;
                valid_selection = true;
            }
            "DEFAULT" => {
                println!();
                println!("You chose the package [default]");
                chosen_packages = Packages::DEFAULT;
                valid_selection = true;
            }
            _ => {
                println!("Invalid Choice [{}]", user_input.trim());
            }
        }
    }

    // Desktop
    valid_selection = false;
    let valid_desktops = vec!["NONE", "DWM", "GNOME", "XFCE", "KDE", "CINNAMON"];
    println!("--");
    println!(
        "Available desktop environments: {}",
        valid_desktops.join(" ").to_uppercase()
    );
    println!();
    while !valid_selection {
        println!("Which desktop environment do you want to install?");
        print!("# ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        user_input = String::new();
        std::io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to read line");
        user_input = user_input.to_uppercase();
        match user_input.trim() {
            "NONE" => {
                println!();
                println!("You chose the desktop environment [none]");
                chosen_desktop = Desktops::NONE;
                valid_selection = true;
            }
            "DWM" => {
                println!();
                println!("You chose the desktop environment [dwm]");
                chosen_desktop = Desktops::DWM;
                valid_selection = true;
            }
            "GNOME" => {
                println!();
                println!("You chose the desktop environment [gnome]");
                chosen_desktop = Desktops::GNOME;
                valid_selection = true;
            }
            "XFCE" => {
                println!();
                println!("You chose the desktop environment [xfce]");
                chosen_desktop = Desktops::XFCE;
                valid_selection = true;
            }
            "KDE" => {
                println!();
                println!("You chose the desktop environment [kde]");
                chosen_desktop = Desktops::KDE;
                valid_selection = true;
            }
            "CINNAMON" => {
                println!();
                println!("You chose the desktop environment [cinnamon]");
                chosen_desktop = Desktops::CINNAMON;
                valid_selection = true;
            }
            _ => {
                println!("Invalid choice [{}]", user_input.trim());
            }
        }
    }

    // Summary
    println!("--");
    println!(
        "You have chosen to install the Package [{:?}] and the Desktop [{:?}]",
        chosen_packages, chosen_desktop
    );
    valid_selection = false;
    while !valid_selection {
        println!("Continue? Y/N");
        print!("# ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        user_input = String::new();
        std::io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to read line");
        user_input = user_input.to_uppercase();
        match user_input.trim() {
            "Y" => {
                valid_selection = true;
            }
            "N" => {
                println!("exit(0)");
                std::process::exit(0);
            }
            _ => {
                println!("Unknown Answer [{}]", user_input.trim());
            }
        }
    }

    return (chosen_desktop, chosen_packages);
}

fn install_arch_base_system() {
    let mut packages: PackageList = vec![];
    let mut services: ServiceList = vec![];

    enable_multilib();
    enable_makepkg_multithreading();
    sync_pacman_mirrors();
    create_reflector_hook();
    install_reflector();
    sync_pacman_mirrors();
    add_arch_base_package_list(&mut packages);
    add_arch_base_service_list(&mut services);

    println!("-- Installing Archlinux Base System --");
    install_package_list(&packages);
    handle_service_list(&services);
}

fn install_reflector() {
    let reflector_package_list: PackageList = vec![
        Package::new("reflector", false, false),
        Package::new("pacman-mirrorlist", false, false),
        Package::new("archlinux-keyring", false, false),
    ];
    install_package_list(&reflector_package_list);
}

fn sync_pacman_mirrors() {
    println!("-- Updating repositories --");
    let output = std::process::Command::new("sudo")
        .args(&["pacman", "-Syy"])
        .stdin(std::process::Stdio::inherit())
        .output()
        .expect("Failed to execute [sync_pacman_mirrors()]");
    if output.status.success() {
        println!("-- Pacman Mirrors have been refreshed --");
    } else {
        println!("ERROR: Failed to refresh Pacman Mirrors");
    }
}

fn execute_post_install_hooks(selected_desktop: &Desktops, selected_packages: &Packages) {
    match selected_packages {
        Packages::NONE => {}
        Packages::MINIMAL => {}
        Packages::DEFAULT => {}
    }

    match selected_desktop {
        Desktops::NONE => {}
        Desktops::DWM => {
            clone_dwm_git_project();
            make_dwm_git_project();
        }
        Desktops::GNOME => {
            disable_gdm_wayland();
        }
        Desktops::XFCE => {}
        Desktops::KDE => {}
        Desktops::CINNAMON => {}
    }
}

fn populate_service_list(
    selected_desktop: &Desktops,
    selected_packages: &Packages,
    service_collection: &mut ServiceList,
) {
    match selected_packages {
        Packages::NONE => {}
        Packages::MINIMAL => {
            add_minimal_service_list(service_collection);
        }
        Packages::DEFAULT => {
            add_minimal_service_list(service_collection);
            add_default_service_list(service_collection);
        }
    }

    match selected_desktop {
        Desktops::NONE => {}
        Desktops::DWM => {
            add_dwm_desktop_service_list(service_collection);
        }
        Desktops::GNOME => {
            add_gnome_desktop_service_list(service_collection);
        }
        Desktops::XFCE => {
            add_xfce_desktop_service_list(service_collection);
        }
        Desktops::KDE => {
            add_kde_desktop_service_list(service_collection);
        }
        Desktops::CINNAMON => {
            add_cinnamon_desktop_service_list(service_collection);
        }
    }
}

fn populate_package_list(
    selected_desktop: &Desktops,
    selected_packages: &Packages,
    package_collection: &mut PackageList,
) {
    match selected_packages {
        Packages::NONE => {}
        Packages::MINIMAL => {
            add_minimal_package_list(package_collection);
        }
        Packages::DEFAULT => {
            add_minimal_package_list(package_collection);
            add_default_package_list(package_collection);
        }
    }

    match selected_desktop {
        Desktops::NONE => {}
        Desktops::DWM => {
            add_dwm_desktop_package_list(package_collection);
        }
        Desktops::GNOME => {
            add_gnome_desktop_package_list(package_collection);
        }
        Desktops::XFCE => {
            add_xfce_desktop_package_list(package_collection);
        }
        Desktops::KDE => {
            add_kde_desktop_package_list(package_collection);
        }
        Desktops::CINNAMON => {
            add_cinnamon_desktop_package_list(package_collection);
        }
    }
}
