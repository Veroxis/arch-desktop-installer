use std::path::Path;

#[derive(Debug)]
pub struct Service<'a> {
    pub name: &'a str,
    pub enabled: bool,
}

impl<'a> Service<'a> {
    pub fn new(name: &str, enabled: bool) -> Service {
        Service {
            name: name,
            enabled,
        }
    }
}

pub type ServiceList<'a> = std::vec::Vec<Service<'a>>;

pub fn handle_service_list(service_list: &ServiceList) {
    for service in service_list {
        if service.enabled {
            enable_service(&service.name);
        } else {
            disable_service(&service.name);
        }
    }
}

fn enable_service(service_name: &str) {
    let output = std::process::Command::new("sudo")
        .args(&["systemctl", "enable", service_name])
        .output()
        .expect(format!("Failed to execute [enable_service({})]", service_name).as_str());
    if output.status.success() {
        println!("Service [{}] was enabled.", service_name);
    } else {
        println!();
        println!(
            "ERROR: Service [{}] failed to be enabled. Code: [{}]",
            service_name,
            output.status.code().unwrap().to_string()
        );
        println!(
            "================================================================================"
        );
        println!(
            "{}",
            std::str::from_utf8(output.stderr.as_slice())
                .unwrap()
                .trim()
        );
        println!(
            "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        );
        println!();
    }
}

fn disable_service(service_name: &str) {
    let service_exists =
        Path::new(format!("/usr/lib/systemd/system/{}.service", service_name).as_str()).exists();
    if service_exists {
        let output = std::process::Command::new("sudo")
            .args(&["systemctl", "disable", service_name])
            .output()
            .expect(format!("Failed to execute [disable_service({})]", service_name).as_str());
        if output.status.success() {
            println!("Service [{}] was disabled.", service_name);
        } else {
            println!();
            println!(
                "ERROR: Service [{}] failed to be disabled. Code: [{}]",
                service_name,
                output.status.code().unwrap().to_string()
            );
            println!(
                "================================================================================"
            );
            println!(
                "{}",
                std::str::from_utf8(output.stderr.as_slice())
                    .unwrap()
                    .trim()
            );
            println!(
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
            );
            println!();
        }
    } else {
        // Services get disabled to prevent conflicts
        // This check is necessary as the disable is fired for some packages which
        // usually aren't installed
        println!(
            "Service [{}] doesn't need to be disabled as it is not installed.",
            service_name
        );
    }
}

pub fn add_arch_base_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(
        vec![
            Service::new("cronie", true),
            Service::new("NetworkManager", true),
            Service::new("earlyoom", true),
        ]
        .into_iter(),
    );
}

pub fn add_minimal_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(
        vec![
            Service::new("avahi-daemon", true),
            Service::new("ufw", true),
        ]
        .into_iter(),
    );
}

pub fn add_default_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(vec![].into_iter());
}

pub fn add_dwm_desktop_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(
        vec![
            Service::new("gdm", false),
            Service::new("lightdm", true),
            Service::new("sddm", false),
        ]
        .into_iter(),
    );
}

pub fn add_gnome_desktop_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(
        vec![
            Service::new("gdm", true),
            Service::new("lightdm", false),
            Service::new("sddm", false),
        ]
        .into_iter(),
    );
}

pub fn add_kde_desktop_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(
        vec![
            Service::new("gdm", false),
            Service::new("lightdm", false),
            Service::new("sddm", true),
        ]
        .into_iter(),
    );
}

pub fn add_xfce_desktop_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(
        vec![
            Service::new("gdm", false),
            Service::new("lightdm", true),
            Service::new("sddm", false),
        ]
        .into_iter(),
    );
}

pub fn add_cinnamon_desktop_service_list(service_collection: &mut ServiceList) {
    service_collection.extend(
        vec![
            Service::new("gdm", false),
            Service::new("lightdm", true),
            Service::new("sddm", false),
        ]
        .into_iter(),
    );
}
