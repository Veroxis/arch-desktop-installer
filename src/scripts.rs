use std::fs::File;
use std::io::{Read, Write};
use std::ops::{Add, AddAssign};
use std::path::Path;

pub fn clone_dwm_git_project() {
	println!("-- Clone dwm git project --");
	let output = std::process::Command::new("git")
		.args(&[
			"clone",
			"https://gitlab.com/Archx/dwm_desktop.git",
			"dwm_desktop",
		])
		.stdin(std::process::Stdio::inherit())
		.stdout(std::process::Stdio::inherit())
		.stderr(std::process::Stdio::inherit())
		.output()
		.expect("Failed to execute clone_dwm_git_project()");
	if !output.status.success() {
		println!("ERROR: Failed to git clone dwm_desktop");
	}
}

pub fn make_dwm_git_project() {
	println!("-- Make dwm_desktop --");
	let output = std::process::Command::new("make")
		.args(&["-C", "dwm_desktop"])
		.stdin(std::process::Stdio::inherit())
		.stdout(std::process::Stdio::inherit())
		.stderr(std::process::Stdio::inherit())
		.output()
		.expect("Failed to execute make_dwm_git_project()");
	if !output.status.success() {
		println!("ERROR: Failed to make dwm_desktop");
	}
}

pub fn disable_gdm_wayland() {
	let conf_file_exists = Path::new("/etc/gdm/custom.conf").exists();
	if conf_file_exists {
		println!("-- Disabling Wayland for GDM --");
		let command =
			format!("sed -i 's/#WaylandEnable=false/WaylandEnable=false/g' /etc/gdm/custom.conf");
		let output = std::process::Command::new("sudo")
			.args(&["bash", "-c", command.as_str()])
			.stdin(std::process::Stdio::inherit())
			.stdout(std::process::Stdio::inherit())
			.stderr(std::process::Stdio::inherit())
			.output()
			.expect("Failed to execute disable_gdm_wayland()");
		if !output.status.success() {
			println!("ERROR: Failed to execute [{}]", command);
		}
	}
}

pub fn create_reflector_hook() {
	println!("-- Creating reflector hook --");
	let mut filecontent = String::new();
	filecontent.add_assign("[Trigger]\n");
	filecontent.add_assign("Operation = Upgrade\n");
	filecontent.add_assign("Type = Package\n");
	filecontent.add_assign("Target = pacman-mirrorlist\n");
	filecontent.add_assign("\n");
	filecontent.add_assign("[Action]\n");
	filecontent.add_assign(
		"Description = Updating pacman-mirrorlist with reflector and removing pacnew...\n",
	);
	filecontent.add_assign("When = PostTransaction\n");
	filecontent.add_assign("Depends = reflector\n");
	filecontent.add_assign("Exec = /bin/sh -c \"reflector --verbose --latest 25 --protocol https --sort rate --country germany --save /etc/pacman.d/mirrorlist; rm -f /etc/pacman.d/mirrorlist.pacnew\"\n");
	create_system_config_file(
		"mirrorupgrade.hook",
		"/etc/pacman.d/hooks/",
		filecontent.as_str(),
		"root",
	);
}

pub fn create_system_config_file(
	filename: &str,
	target_path: &str,
	file_content: &str,
	owner: &str,
) {
	let output = std::process::Command::new("sudo")
		.args(&["mkdir", "-p", target_path])
		.output()
		.expect(format!("Failed to execute 'mkdir -p {}'", target_path).as_str());
	if !output.status.success() {
		panic!("ERROR: Failed to create directory {}", target_path);
	}
	let absolute_target_path = String::from(target_path).add("/").add(filename);
	let mut temp_dir = std::env::temp_dir();
	temp_dir.push(filename);
	let mut file = File::create(&temp_dir).unwrap();
	file.write_all(file_content.as_ref()).unwrap();
	let temp_file_path_string = String::from(temp_dir.as_os_str().to_str().unwrap());
	let output = std::process::Command::new("sudo")
		.args(&["mv", temp_file_path_string.as_str(), target_path])
		.stdin(std::process::Stdio::inherit())
		.stdout(std::process::Stdio::inherit())
		.stderr(std::process::Stdio::inherit())
		.output()
		.expect(
			format!(
				"Failed to execute 'sudo mv {} {}'",
				temp_file_path_string, absolute_target_path
			)
			.as_str(),
		);
	if !output.status.success() {
		panic!(
			"{} couldn't be moved to {}",
			temp_file_path_string, absolute_target_path
		);
	}
	let output = std::process::Command::new("sudo")
		.args(&[
			"chown",
			format!("{0}:{0}", owner).as_str(),
			absolute_target_path.as_str(),
		])
		.stdin(std::process::Stdio::inherit())
		.stdout(std::process::Stdio::inherit())
		.stderr(std::process::Stdio::inherit())
		.output()
		.expect(
			format!(
				"Failed to execute 'sudo chown {0}:{0} {1}'",
				owner, absolute_target_path
			)
			.as_str(),
		);
	if !output.status.success() {
		panic!("ERROR: Couldn't change owner of {}", absolute_target_path);
	}
}

pub fn enable_makepkg_multithreading() {
	println!("-- Enabling makepkg multithreading --");
	let output = std::process::Command::new("sudo")
		.args(&[
			"sed",
			"-i",
			"s/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -z --threads=0 -)/g",
			"/etc/makepkg.conf",
		])
		.stdin(std::process::Stdio::inherit())
		.stdout(std::process::Stdio::inherit())
		.stderr(std::process::Stdio::inherit())
		.output()
		.expect("Failed to execute [enable_makepkg_multithreading()]");
	if !output.status.success() {
		panic!("ERROR: Failed to enable makepkg multithreading");
	}
}

pub fn enable_multilib() {
	println!("-- Enabling multilib repository --");
	let mut file = File::open("/etc/pacman.conf").expect("Unable to open /etc/pacman.conf");
	let mut contents = String::new();
	file.read_to_string(&mut contents)
		.expect("Unable to read the file");
	let mut new_contents = String::new();
	let mut found_multilib = false;
	contents.lines().into_iter().for_each(|line| {
		let mut line_string = String::from(line);
		if found_multilib {
			if line_string.trim() == "#Include = /etc/pacman.d/mirrorlist" {
				line_string = "Include = /etc/pacman.d/mirrorlist".to_string();
			}
			found_multilib = false;
		}
		if line.trim() == "#[multilib]" {
			line_string = "[multilib]".to_string();
			found_multilib = true;
		}
		line_string.add_assign("\n");
		new_contents.add_assign(line_string.as_str());
	});
	create_system_config_file("pacman.conf", "/etc", new_contents.as_str(), "root");
}

pub fn install_yay_package_manager() {
	let yay_exists = Path::new("/usr/bin/yay").exists();
	if !yay_exists {
		println!("-- Installing yay --");
		let yay_install_dir = String::from(std::env::temp_dir().to_str().unwrap()).add("/yay");
		let yay_install_dir_exists = Path::new(yay_install_dir.as_str()).exists();
		if yay_install_dir_exists {
			std::fs::remove_dir_all(yay_install_dir.as_str()).unwrap();
		}
		let mut cmd = std::process::Command::new("git");
		cmd.args(&[
			"clone",
			"https://aur.archlinux.org/yay-bin.git",
			yay_install_dir.as_str(),
		]);
		cmd.stdin(std::process::Stdio::inherit());
		if std::env::args().any(|arg| arg == "-v" || arg == "--verbose") {
			cmd.stdout(std::process::Stdio::inherit());
			cmd.stderr(std::process::Stdio::inherit());
		}
		let output = cmd
			.output()
			.expect("Failed to clone 'yay' from aur.archlinux.org");
		if !output.status.success() {
			panic!("Cant clone 'yay'");
		}
		let install_command = format!(
			"cd {} && makepkg -sic --noconfirm --needed",
			yay_install_dir
		);
		let mut cmd = std::process::Command::new("sh");
		cmd.args(&["-c", install_command.as_str()]);
		cmd.stdin(std::process::Stdio::inherit());
		if std::env::args().any(|arg| arg == "-v" || arg == "--verbose") {
			cmd.stdout(std::process::Stdio::inherit());
			cmd.stderr(std::process::Stdio::inherit());
		}
		cmd.output().expect("Failed to change directory");
	}
}
