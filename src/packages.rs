#[derive(Debug)]
pub struct Package<'a> {
    pub name: &'a str,
    pub is_package_group: bool,
    pub from_aur: bool,
}

impl<'a> Package<'a> {
    pub fn new(name: &str, is_package_group: bool, from_aur: bool) -> Package {
        Package {
            name: name,
            is_package_group,
            from_aur,
        }
    }
}

pub type PackageList<'a> = std::vec::Vec<Package<'a>>;

pub fn install_package_list(package_list: &PackageList) {
    let mut official_package_names: std::vec::Vec<String> = Vec::new();
    let mut aur_package_names: std::vec::Vec<String> = Vec::new();
    for package in package_list {
        if package.from_aur {
            aur_package_names.push(package.name.to_string());
        } else {
            official_package_names.push(package.name.to_string());
        }
    }

    let amount_official_packages: u32 = official_package_names.len() as u32;
    let amount_aur_packages: u32 = aur_package_names.len() as u32;

    if amount_official_packages > 0 {
        println!(
            "<< Installing packages from official repositories [{}] >>",
            amount_official_packages
        );
        println!("Packages: [{}]", official_package_names.join(" "));
        let mut cmd = std::process::Command::new("yay");
        cmd.args(&["-Syyuu", "--needed", "--noconfirm", "--sudoloop"]);
        cmd.args(&official_package_names);
        cmd.stdin(std::process::Stdio::inherit());
        if std::env::args().any(|arg| arg == "-v" || arg == "--verbose") {
            cmd.stdout(std::process::Stdio::inherit());
            cmd.stderr(std::process::Stdio::inherit());
        }
        let output = cmd.output().expect("Failed to execute [pacman]");
        if !output.status.success() {
            println!();
            println!("ERROR: Failed to install the official repository packages");
            println!(
                "================================================================================"
            );
            println!(
                "{}",
                std::str::from_utf8(output.stderr.as_slice())
                    .unwrap()
                    .trim()
            );
            println!(
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
            );
            println!();
        }
    }

    if amount_aur_packages > 0 {
        println!(
            "<< Installing packages from the AUR [{}] >>",
            amount_aur_packages
        );
        let mut iteration_count: usize = 0;
        for package in aur_package_names {
            iteration_count = iteration_count + 1;
            println!(
                "[{} of {}] Installing [{}]",
                iteration_count, amount_aur_packages, package
            );
            let mut cmd = std::process::Command::new("yay");
            cmd.args(&[
                "-S",
                "--needed",
                "--noconfirm",
                "--sudoloop",
                package.as_str(),
            ]);
            cmd.stdin(std::process::Stdio::inherit());
            if std::env::args().any(|arg| arg == "-v" || arg == "--verbose") {
                cmd.stdout(std::process::Stdio::inherit());
                cmd.stderr(std::process::Stdio::inherit());
            }
            let output = cmd.output().expect("Failed to execute [pacman]");
            if !output.status.success() {
                println!();
                println!(
                    "ERROR: Failed to install package [{}] from the AUR",
                    package
                );
                println!("================================================================================");
                println!(
                    "{}",
                    std::str::from_utf8(output.stderr.as_slice())
                        .unwrap()
                        .trim()
                );
                println!("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                println!();
            }
        }
    }

    println!("<< Done >>");
}

pub fn add_arch_base_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            Package::new("base", false, false),
            Package::new("base-devel", false, false),
            Package::new("arch-install-scripts", false, false),
            Package::new("netctl", false, false),
            Package::new("dialog", false, false),
            Package::new("linux-zen", false, false),
            Package::new("linux-zen-headers", false, false),
            Package::new("linux-firmware", false, false),
            Package::new("reflector", false, false),
            Package::new("git", false, false),
            Package::new("wget", false, false),
            Package::new("pacman-contrib", false, false),
            Package::new("rsync", false, false),
            Package::new("openssh", false, false),
            Package::new("networkmanager", false, false),
            Package::new("cronie", false, false),
            Package::new("bash-completion", false, false),
            Package::new("earlyoom", false, false),
        ]
        .into_iter(),
    );
}

pub fn add_minimal_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            // Fonts
            Package::new("noto-fonts", false, false),
            // Fonts
            Package::new("noto-fonts", false, false),
            Package::new("noto-fonts-extra", false, false),
            Package::new("steam-fonts", false, true),
            Package::new("wqy-zenhei", false, false),
            Package::new("ttf-ubuntu-font-family", false, false),
            Package::new("noto-fonts-emoji", false, false),
            Package::new("otf-latin-modern", false, false),
            Package::new("otf-latinmodern-math", false, false),
            Package::new("gnu-free-fonts", false, false),
            Package::new("ttf-arphic-uming", false, false),
            Package::new("ttf-indic-otf", false, false),
            Package::new("ttf-liberation", false, false),
            Package::new("ttf-dejavu", false, false),
            Package::new("powerline-fonts", false, false),
            Package::new("ttf-font-awesome", false, false),
            // pulseaudio
            // Package::new("pipewire", false, false),
            // Package::new("pipewire-alsa", false, false),
            // Package::new("pipewire-pulse", false, false),
            // Package::new("pipewire-jack", false, false),
            Package::new("pulseaudio", false, false),
            Package::new("pulseaudio-alsa", false, false),
            Package::new("pulseaudio-bluetooth", false, false),
            Package::new("pulseaudio-equalizer", false, false),
            Package::new("pulseaudio-jack", false, false),
            Package::new("pulseaudio-lirc", false, false),
            Package::new("pulseaudio-zeroconf", false, false),
            Package::new("pavucontrol", false, false),
            Package::new("alsa-utils", false, false),
            // gparted
            Package::new("gparted", false, false),
            Package::new("ntfs-3g", false, false),
            Package::new("dosfstools", false, false),
            Package::new("exfat-utils", false, false),
            Package::new("gpart", false, false),
            Package::new("mtools", false, false),
            // printer
            Package::new("cups", false, false),
            Package::new("cups-pdf", false, false),
            Package::new("gutenprint", false, false),
            // Theming
            Package::new("papirus-icon-theme", false, false),
            Package::new("matcha-gtk-theme", false, true),
            // Firewall
            Package::new("gufw", false, false),
            // Network Demon
            Package::new("avahi", false, false),
            // Application Editor
            Package::new("menulibre", false, true),
            // Package Manager
            Package::new("flatpak", false, false),
            // System Monitor
            Package::new("htop", false, false),
            Package::new("neofetch", false, false),
            // Media Codecs
            Package::new("gst-plugins-base", false, false),
            Package::new("lib32-gst-plugins-base", false, false),
            Package::new("gst-plugins-good", false, false),
            Package::new("lib32-gst-plugins-good", false, false),
            Package::new("gst-plugins-bad", false, false),
            Package::new("gst-plugins-ugly", false, false),
            // Power Optimizer
            Package::new("tlp", false, false),
            Package::new("tlpui-git", false, true),
            // Shell Tools
            Package::new("fzf", false, false),
            Package::new("z", false, false),
            Package::new("skim", false, false),
        ]
        .into_iter(),
    );
}

pub fn add_default_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            // Package Manager
            Package::new("pamac-aur", false, true),
            // System Monitor
            Package::new("gnome-disk-utility", false, false),
            Package::new("baobab", false, false),
            Package::new("gnome-logs", false, false),
            // Office Suite
            Package::new("libreoffice-fresh", false, false),
            // Video Player
            Package::new("vlc", false, false),
            Package::new("celluloid", false, false),
            // Wine
            Package::new("wine-staging", false, false),
            Package::new("giflib", false, false),
            Package::new("lib32-giflib", false, false),
            Package::new("libpng", false, false),
            Package::new("lib32-libpng", false, false),
            Package::new("libldap", false, false),
            Package::new("lib32-libldap", false, false),
            Package::new("gnutls", false, false),
            Package::new("lib32-gnutls", false, false),
            Package::new("mpg123", false, false),
            Package::new("lib32-mpg123", false, false),
            Package::new("openal", false, false),
            Package::new("lib32-openal", false, false),
            Package::new("v4l-utils", false, false),
            Package::new("lib32-v4l-utils", false, false),
            Package::new("libpulse", false, false),
            Package::new("lib32-libpulse", false, false),
            Package::new("libgpg-error", false, false),
            Package::new("lib32-libgpg-error", false, false),
            Package::new("alsa-plugins", false, false),
            Package::new("lib32-alsa-plugins", false, false),
            Package::new("alsa-lib", false, false),
            Package::new("lib32-alsa-lib", false, false),
            Package::new("libjpeg-turbo", false, false),
            Package::new("lib32-libjpeg-turbo", false, false),
            Package::new("sqlite", false, false),
            Package::new("lib32-sqlite", false, false),
            Package::new("libxcomposite", false, false),
            Package::new("lib32-libxcomposite", false, false),
            Package::new("libxinerama", false, false),
            Package::new("lib32-libgcrypt", false, false),
            Package::new("libgcrypt", false, false),
            Package::new("lib32-libxinerama", false, false),
            Package::new("ncurses", false, false),
            Package::new("lib32-ncurses", false, false),
            Package::new("ocl-icd", false, false),
            Package::new("lib32-ocl-icd", false, false),
            Package::new("libxslt", false, false),
            Package::new("lib32-libxslt", false, false),
            Package::new("libva", false, false),
            Package::new("lib32-libva", false, false),
            Package::new("gtk3", false, false),
            Package::new("lib32-gtk3", false, false),
            Package::new("gst-plugins-base-libs", false, false),
            Package::new("lib32-gst-plugins-base-libs", false, false),
            Package::new("vulkan-icd-loader", false, false),
            Package::new("lib32-vulkan-icd-loader", false, false),
            // Lutris
            Package::new("lutris", false, false),
            Package::new("gamemode", false, false),
            Package::new("lib32-gamemode", false, false),
            Package::new("lib32-libcanberra", false, false),
            Package::new("lib32-libcurl-compat", false, false),
            Package::new("libcurl-compat", false, false),
            Package::new("lib32-mesa", false, false),
            Package::new("mesa", false, false),
            // Internet Browser
            Package::new("firefox", false, false),
            // Image Editor
            Package::new("gimp", false, false),
            // Video Editor
            Package::new("blender", false, false),
            // Game Engine
            Package::new("godot", false, false),
            // PDF Viewer
            Package::new("evince", false, false),
        ]
        .into_iter(),
    );
}

pub fn add_dwm_desktop_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            // Xorg
            Package::new("xorg-server", false, false),
            Package::new("xorg-xinit", false, false),
            Package::new("xorg-xrandr", false, false),
            Package::new("xorg-xsetroot", false, false),
            Package::new("xdotool", false, false),
            Package::new("bc", false, false),
            // Lightdm
            Package::new("lightdm", false, false),
            Package::new("lightdm-gtk-greeter", false, false),
            Package::new("lightdm-gtk-greeter-settings", false, false),
            // Desktop utils from LXQT Desktop
            Package::new("lxqt", true, false),
            Package::new("breeze-icons", false, false),
            Package::new("oxygen-icons", false, false),
            // Background renderer
            Package::new("nitrogen", false, false),
            // Compositor
            Package::new("picom", false, false),
            Package::new("xcompmgr", false, false),
            // XDG Autostart
            Package::new("dex", false, false),
            // Application Menu
            Package::new("jgmenu", false, false),
            // File Manager
            Package::new("nemo", false, false),
            Package::new("nemo-fileroller", false, false),
            // Network Manager
            Package::new("network-manager-applet", false, false),
            Package::new("nm-connection-editor", false, false),
            // Clipboard Manager
            Package::new("autocutsel", false, false),
            // Lock Screen
            Package::new("physlock", false, false),
            // Sound control
            Package::new("pamixer", false, false),
            Package::new("pasystray", false, false),
            // Cursor Theme
            Package::new("xcursor-vanilla-dmz", false, false),
        ]
        .into_iter(),
    );
}

pub fn add_gnome_desktop_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            // Base Desktop
            Package::new("xorg", true, false),
            Package::new("gnome", true, false),
            Package::new("gnome-tweaks", false, false),
            // Shell Extensions
            Package::new("chrome-gnome-shell", false, false),
            Package::new("gnome-shell-extensions", false, false),
            Package::new("gnome-shell-extension-appindicator-git", false, true),
            Package::new("gnome-shell-extension-arc-menu-git", false, true),
            Package::new("gnome-shell-extension-coverflow-alt-tab-git", false, true),
            Package::new("gnome-shell-extension-dash-to-panel-git", false, true),
            Package::new("gnome-shell-extension-sound-output-device-chooser", false, true),
            Package::new("gnome-shell-extension-tray-icons-reloaded-git", false, true),
        ]
        .into_iter(),
    );
}

pub fn add_kde_desktop_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            // Base Desktop
            Package::new("plasma", false, false),
            Package::new("sddm", false, false),
            Package::new("xsettingsd", false, false),
            Package::new("packagekit-qt5", false, false),
            // File Manager
            Package::new("dolphin", false, false),
            // Terminal Emulator
            Package::new("konsole", false, false),
        ]
        .into_iter(),
    );
}

pub fn add_xfce_desktop_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            // Base Desktop
            Package::new("xorg", true, false),
            Package::new("xfce4", true, false),
            Package::new("xfce4-goodies", true, false),
            Package::new("gvfs", false, false),
            Package::new("network-manager-applet", false, false),
            Package::new("nm-connection-editor", false, false),
            Package::new("mugshot", false, true),
            Package::new("file-roller", false, false),
            // Dashboard for usability
            Package::new("xfdashboard", false, true),
            // Login Manager
            Package::new("lightdm", false, false),
            Package::new("lightdm-gtk-greeter", false, false),
            Package::new("lightdm-gtk-greeter-settings", false, false),
        ]
        .into_iter(),
    );
}

pub fn add_cinnamon_desktop_package_list(package_collection: &mut PackageList) {
    package_collection.extend(
        vec![
            // Base Desktop
            Package::new("xorg", true, false),
            Package::new("cinnamon", false, false),
            Package::new("gnome-keyring", false, false),
            Package::new("gnome-screenshot", false, false),
            Package::new("gnome-calculator", false, false),
            Package::new("seahorse", false, false),
            // Login Manager
            Package::new("lightdm", false, false),
            Package::new("lightdm-gtk-greeter", false, false),
            Package::new("lightdm-gtk-greeter-settings", false, false),
        ]
        .into_iter(),
    );
}
